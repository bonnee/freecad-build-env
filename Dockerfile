FROM archlinux/archlinux:latest

SHELL ["/bin/bash", "-c"]

ADD add_files/freecad_build_script.sh /root/build_script.sh
RUN pacman -Syu --noconfirm
RUN pacman -S --noconfirm base-devel cmake ninja boost desktop-file-utils eigen gcc-fortran swig xerces-c coin openmpi
RUN pacman -S --noconfirm vim boost-libs curl opencascade>=7.2 xerces-c libspnav glew netcdf utf8cpp shared-mime-info hicolor-icon-theme jsoncpp qt5-base qt5-declarative qt5-svg qt5-tools qt5-x11extras qt5-xmlpatterns qt5-webkit med python-pivy python-pyside2 python-matplotlib pyside2-tools shiboken2
RUN pacman -Scc --noconfirm
WORKDIR /root

ENTRYPOINT ["./build_script.sh"]

